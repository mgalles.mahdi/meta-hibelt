SUMMARY = "Master recipe"
DESCRIPTION = " "
LICENSE = "CLOSED"
PR = "r3"
M_S_DEVICE= "MASTER"
S = "${WORKDIR}/Master/Source/"
ROOT_DIR = "/home/root"
SYSTMD_DIR = "/etc/systemd/system"
USR_SHARE_DIR = "/usr/share/X11/xorg.conf.d/"
IBELT_DEMO_V1.1_DIR="${ROOT_DIR}/iBELT_DEMO_v1.1"
SRC_URI =  " \
    file://Master/home/root/can_config.sh \
    file://Master/home/root/connexion_fibocom.sh \
    file://Master/home/root/ipFixe.sh \
    file://Master/home/root/rotate.sh \
    file://Master/Services/init_can.service \
    file://Master/Services/init_fibocom.service \
    file://Master/Services/init_ipFixe.service \
    file://Master/Services/init_rotate_screen.service \
    file://Master/Services/start_demo_master.service \
    file://Master/x11/10-quirks.conf \
    file://Master/Source/connexion_fibocom.c \
    file://Master/home/root/iBELT_DEMO_v1.1/iBELT_GUI \
    file://Master/home/root/iBELT_DEMO_v1.1/start_master_can0.sh \
"

do_install () {

    install -d ${D}${SYSTMD_DIR}
    install -m 0644 ${WORKDIR}/Master/Services/*.service ${D}${SYSTMD_DIR}

    install -d ${D}${ROOT_DIR}
    install -m 0777 ${WORKDIR}/Master/home/root/*.sh ${D}${ROOT_DIR}
    ${CC} ${LDFLAGS} ${S}/connexion_fibocom.c -o ${S}/connexion_fibocom
    install -m 0777 ${S}/connexion_fibocom ${D}${ROOT_DIR}

    install -d ${D}${USR_SHARE_DIR}
    install -m 0644 ${WORKDIR}/Master/x11/*.conf ${D}${USR_SHARE_DIR}
    install -d ${D}${IBELT_DEMO_V1.1_DIR}
    install -m 0644 ${WORKDIR}/Master/home/root/iBELT_DEMO_v1.1/iBELT_GUI ${D}${IBELT_DEMO_V1.1_DIR}
    install -m 0777 ${WORKDIR}/Master/home/root/iBELT_DEMO_v1.1/start_master_can0.sh ${D}${IBELT_DEMO_V1.1_DIR}

}

NATIVE_SYSTEMD_SUPPORT = "1"
SYSTEMD_PACKAGES = "${PN}"
SYSTEMD_SERVICE_${PN} = "init_can.service"
SYSTEMD_SERVICE_${PN} += "init_fibocom.service"
SYSTEMD_SERVICE_${PN} += "init_ipFixe.service"
SYSTEMD_SERVICE_${PN} += "init_rotate_screen.service"
SYSTEMD_SERVICE_${PN} += "start_demo_master.service"
FILES_${PN} += "${ROOT_DIR}/*"
FILES_${PN} += "${SYSTMD_DIR}/*"
FILES_${PN} += "${USR_SHARE_DIR}/*"
FILES_${PN} += "${IBELT_DEMO_V1.1_DIR}/*"
inherit systemd

