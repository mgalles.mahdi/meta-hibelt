#!/bin/sh

# X11 DISPLAY
export DISPLAY=:0.0                                                       


# COMS UART device
COMS_UART=/dev/ttymxc2

echo " >> Start iBELT slave application"
cd /home/root/iBELT_DEMO_v1.1
./iBELT_GUI S $COMS_UART 0 0 nocan 0
