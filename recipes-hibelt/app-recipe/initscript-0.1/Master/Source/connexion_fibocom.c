    #include <stdio.h>
    #include <fcntl.h>
    #include <termios.h>
    #include <unistd.h>
    #include <errno.h>
    #include <string.h>
    #include <stdarg.h>
    #include <time.h>
    #define FIBOCOM_DEVICE ("/dev/ttyACM0")
    #define AT_COMMAND ("AT\r")
    #define CPIN_COMMAND ("AT+CPIN?\r")
    #define CGREG_COMMAND ("AT+CGREG?\r")
    #define COPS_COMMAND ("AT+COPS?\r")
    #define CSQ_COMMAND ("AT+CSQ\r")
    #define CGATT_COMMAND ("AT+CGATT=1\r")
    #define XDATACHANNEL_COMMAND                                                   \
      ("AT+XDATACHANNEL=1,1,\"/USBCDC/0\",\"/USBHS/NCM/0\",2,0\r")
    #define CGDATA_COMMAND ("AT+CGDATA=\"M-RAW_IP\",0\r")
    #define CGPADDR_COMMAND ("AT+CGPADDR=0\r")
    #define XDNS_COMMAND ("AT+XDNS?\r")
    #define BAUDRATE B38400
    #define PARAMS __FUNCTION__,__LINE__
    #define SUCCESS (0)
    #define ERROR (1)
    #define FAIL (2)
    
    void
    ibelt_log (const char *fnc, int ligne, char *msg, ...)
    {
    
      struct tm *local;
      time_t t = time (NULL);
      local = localtime (&t);
      char str_time[100] = { 0 };
      va_list args;
    
      va_start (args, msg);
      vprintf (msg, args);
      va_end (args);
      va_start (args, msg);
      snprintf (str_time, sizeof (str_time), ctime (&t));
      str_time[strlen (str_time) - 1] = '\0';
      printf ("[%s][%s:%d]:", str_time, fnc, ligne);
      vprintf (msg, args);
      va_end (args);
      printf ("\n");
      return;
    }
    
    
    int
    send_XDNS_COMMAND (int fd)
    {
      unsigned char buffer[1000] = { 0 };
      int nb_read = 0;
      int nb_write = -1;
      int i = 0;
      int rep = -1;
      int reading = 1;
      int response = FAIL;
      char dns1[50] = { 0 };
      char dns2[50] = { 0 };
      char *ptr_buffer = NULL;
      FILE *dns1_file = NULL;
      FILE *dns2_file = NULL;
    
      nb_write = write (fd, XDNS_COMMAND, sizeof (XDNS_COMMAND));
      if (nb_write < 1)
        {
          ibelt_log (PARAMS, "erreur de l'ecriture de XDNS_COMMAND\n");
          return 1;
        }
      tcflush (fd, TCIFLUSH);	/* Discabufferrds old data in the rx buffer */
    
      while (reading && nb_read < sizeof (buffer))
        {
    
          rep = read (fd, buffer + nb_read, sizeof (char));
          if (rep < 1)
        {
          break;
        }
          if (*(buffer + nb_read) == '\0')
        {
          *(buffer + nb_read) = '\r';
        }
          buffer[++nb_read] = '\0';
    
          if (strstr (buffer, "OK"))
        {
          ibelt_log (PARAMS, "buffer = %s\n", buffer);
          ibelt_log (PARAMS, "la commande XDNS_COMMAND est OK \n");
          reading = 0;
          response = SUCCESS;
        }
          else if (strstr (buffer, "ERROR"))
        {
          ibelt_log (PARAMS, "buffer = %s\n", buffer);
          ibelt_log (PARAMS, "la commande XDNS_COMMAND est KO \n");
          reading = 0;
          response = ERROR;
        }
        }
      if (response == SUCCESS)
        {
          ptr_buffer = strstr (buffer, "+XDNS: 0, \"");
          if (ptr_buffer)
        {
          ptr_buffer += strlen ("+XDNS: 0, \"");
          i = 0;
          while (*ptr_buffer != '"')
            {
              if (*ptr_buffer == '\0')
            {
              ibelt_log (PARAMS,
                     "le buffer ne contient pas tous les info \n");
              return ERROR;
            }
              *(dns1 + i++) = *ptr_buffer++;
            }
          dns1[i] = '\0';
        }
          else
        {
          ibelt_log (PARAMS, "erreur buffer \n");
          return ERROR;;
        }
          dns1_file = fopen ("/tmp/dns1.log", "w");
          if (dns1_file == NULL)
        {
          ibelt_log (PARAMS, "erreur ouverture du fichier /tmp/dns1.log\n");
          return ERROR;
        }
          fprintf (dns1_file, dns1);
          fclose (dns1_file);
    
          i = 0;
          ptr_buffer += strlen ("\", \'");
          while (*ptr_buffer != '"')
        {
          if (*ptr_buffer == '\0')
            {
              ibelt_log (PARAMS,
                 "le buffer ne contient pas tous les info \n");
              return ERROR;
            }
          *(dns2 + i++) = *ptr_buffer++;
        }
          dns2[i] = '\0';
          dns2_file = fopen ("/tmp/dns2.log", "w");
          if (dns2_file == NULL)
        {
          ibelt_log (PARAMS, "erreur ouverture du fichier /tmp/dns2.log\n");
          return ERROR;
        }
          fprintf (dns2_file, dns2);
          fclose (dns2_file);
    
        }
      else
        {
          ibelt_log (PARAMS, "erreur de la lecture de XDNS_COMMAND\n");
          return ERROR;
        }
      return SUCCESS;
    }
    
    int
    send_CGPADDR_COMMAND (int fd)
    {
      unsigned char buffer[1000] = { 0 };
      int nb_read = 0;
      int nb_write = -1;
      int i = 0;
      int rep = -1;
      int reading = 1;
      int response = FAIL;
      char ip[50] = { 0 };
      char *ptr_buffer = NULL;
      FILE *ip_file = NULL;
      int get_ip = 0;
    
      nb_write = write (fd, CGPADDR_COMMAND, sizeof (CGPADDR_COMMAND));
      if (nb_write < 1)
        {
          ibelt_log (PARAMS, "erreur de l'ecriture de CGPADDR_COMMAND\n");
          return 1;
        }
      tcflush (fd, TCIFLUSH);	/* Discabufferrds old data in the rx buffer */
    
      while (reading && nb_read < sizeof (buffer))
        {
    
          rep = read (fd, buffer + nb_read, sizeof (char));
          if (rep < 1)
        {
          break;
        }
          if (*(buffer + nb_read) == '\0')
        {
          *(buffer + nb_read) = '\r';
        }
          buffer[++nb_read] = '\0';
    
          if (strstr (buffer, "OK"))
        {
          ibelt_log (PARAMS, "buffer = %s\n", buffer);
          ibelt_log (PARAMS, "la commande CGPADDR_COMMAND est OK \n");
          ptr_buffer = strstr (buffer, "+CGPADDR: 0,\"");
          response = SUCCESS;
          reading = 0;
        }
          else if (strstr (buffer, "ERROR"))
        {
          ibelt_log (PARAMS, "buffer = %s\n", buffer);
          ibelt_log (PARAMS, "la commande CGPADDR_COMMAND est KO \n");
          reading = 0;
          response = ERROR;
        }
        }
      if (ptr_buffer)
        {
          ptr_buffer += strlen ("+CGPADDR: 0,\"");
          while (*ptr_buffer != '"')
        {
          if (*ptr_buffer == '\0')
            {
              ibelt_log (PARAMS,
                 "le buffer ne contient pas toutes les info \n");
              response = ERROR;
              break;
            }
          *(ip + i++) = *ptr_buffer++;
        }
          if (SUCCESS == response)
        {
          ip[i] = '\0';
          ip_file = fopen ("/tmp/ipfile.log", "w");
          if (ip_file == NULL)
            {
              ibelt_log (PARAMS,
                 "erreur ouverture du fichier /tmp/ipfile.log\n");
            }
          fprintf (ip_file, ip);
          fclose (ip_file);
        }
        }
      return response;
    }
    
    int
    send_CGDATA_COMMAND (int fd)
    {
      unsigned char buffer[1000] = { 0 };
      int nb_read = 0;
      int nb_write = -1;
      int i = 0;
      int rep = -1;
      int reading = 1;
      int response = FAIL;
      nb_write = write (fd, CGDATA_COMMAND, sizeof (CGDATA_COMMAND));
      if (nb_write < 1)
        {
          ibelt_log (PARAMS, "erreur de l'ecriture de CGDATA\n");
          return 1;
        }
      tcflush (fd, TCIFLUSH);	/* Discabufferrds old data in the rx buffer */
    
    
      while (reading && nb_read < sizeof (buffer))
        {
    
          rep = read (fd, buffer + nb_read, sizeof (char));
          if (rep < 1)
        {
          break;
        }
          if (*(buffer + nb_read) == '\0')
        {
          *(buffer + nb_read) = '\r';
        }
          buffer[++nb_read] = '\0';
    
          if (strstr (buffer, "OK") && strstr (buffer, "AT+CGDATA=\"M-RAW_IP\",0")
          && strstr (buffer, "CONNECT"))
        {
          ibelt_log (PARAMS, "buffer = %s\n", buffer);
          ibelt_log (PARAMS, "la commande CGDATA est OK \n");
          reading = 0;
          response = SUCCESS;
        }
          else if (strstr (buffer, "ERROR"))
        {
          ibelt_log (PARAMS, "buffer = %s\n", buffer);
          ibelt_log (PARAMS, "la commande CGDATA est KO \n");
          reading = 0;
          response = ERROR;
        }
        }
      return response;
    }
    
    int
    send_XDATACHANNEL_COMMAND (int fd)
    {
      unsigned char buffer[1000] = { 0 };
      int nb_read = 0;
      int nb_write = -1;
      int i = 0;
      int rep = -1;
      int reading = 1;
      int response = FAIL;
      nb_write = write (fd, XDATACHANNEL_COMMAND, sizeof (XDATACHANNEL_COMMAND));
      if (nb_write < 1)
        {
          ibelt_log (PARAMS, "erreur de l'ecriture de XDATACHANNEL\n");
          return 1;
        }
      tcflush (fd, TCIFLUSH);	/* Discabufferrds old data in the rx buffer */
    
    
      while (reading && nb_read < sizeof (buffer))
        {
    
          rep = read (fd, buffer + nb_read, sizeof (char));
          if (rep < 1)
        {
          break;
        }
          if (*(buffer + nb_read) == '\0')
        {
          *(buffer + nb_read) = '\r';
        }
          buffer[++nb_read] = '\0';
    
          if (strstr
          (buffer, "AT+XDATACHANNEL=1,1,\"/USBCDC/0\",\"/USBHS/NCM/0\",2,0")
          && strstr (buffer, "OK"))
        {
          ibelt_log (PARAMS, "buffer = %s\n", buffer);
          ibelt_log (PARAMS, "la commande XDATACHANNEL est OK \n");
          reading = 0;
          response = SUCCESS;
        }
          else if (strstr (buffer, "ERROR"))
        {
          ibelt_log (PARAMS, "buffer = %s\n", buffer);
          ibelt_log (PARAMS, "la commande XDATACHANNEL est KO \n");
          reading = 0;
          response = ERROR;
        }
        }
      return response;
    }
    
    int
    send_CSQ_COMMAND (int fd)
    {
      unsigned char buffer[1000] = { 0 };
      int nb_read = 0;
      int nb_write = -1;
      int i = 0;
      int rep = -1;
      int reading = 1;
      int response = FAIL;
      nb_write = write (fd, CSQ_COMMAND, sizeof (CSQ_COMMAND));
      if (nb_write < 1)
        {
          ibelt_log (PARAMS, "erreur de l'ecriture de CSQ\n");
          return 1;
        }
      tcflush (fd, TCIFLUSH);	/* Discabufferrds old data in the rx buffer */
    
    
      while (reading && nb_read < sizeof (buffer))
        {
    
          rep = read (fd, buffer + nb_read, sizeof (char));
          if (rep < 1)
        {
          break;
        }
          if (*(buffer + nb_read) == '\0')
        {
          *(buffer + nb_read) = '\r';
        }
          buffer[++nb_read] = '\0';
    
          if (strstr (buffer, "OK") && strstr (buffer, "+CSQ:"))
        {
          ibelt_log (PARAMS, "buffer = %s\n", buffer);
          ibelt_log (PARAMS, "la commande CSQ est OK \n");
          reading = 0;
          response = SUCCESS;
        }
          else if (strstr (buffer, "ERROR"))
        {
          ibelt_log (PARAMS, "buffer = %s\n", buffer);
          ibelt_log (PARAMS, "la commande CSQ est KO \n");
          reading = 0;
          response = ERROR;
        }
        }
      return response;
    }
    
    int
    send_CGATT_COMMAND (int fd)
    {
    
      unsigned char buffer[1000] = { 0 };
      int nb_read = 0;
      int nb_write = -1;
      int i = 0;
      int rep = -1;
      int reading = 1;
      int response = FAIL;
      nb_write = write (fd, CGATT_COMMAND, sizeof (CGATT_COMMAND));
      if (nb_write < 1)
        {
          ibelt_log (PARAMS, "erreur de l'ecriture de CGATT\n");
          return 1;
        }
      tcflush (fd, TCIFLUSH);	// Discabufferrds old data in the rx buffer
    
    
      while (reading && nb_read < sizeof (buffer))
        {
    
          rep = read (fd, buffer + nb_read, sizeof (char));
          if (rep < 1)
        {
          break;
        }
          if (*(buffer + nb_read) == '\0')
        {
          *(buffer + nb_read) = '\r';
        }
          buffer[++nb_read] = '\0';
    
          if (strstr (buffer, "OK") && strstr (buffer, "CGATT"))
        {
          ibelt_log (PARAMS, "buffer = %s\n", buffer);
          ibelt_log (PARAMS, "la commande CGATT est OK \n");
          reading = 0;
          response = SUCCESS;
        }
          else if (strstr (buffer, "ERROR"))
        {
          ibelt_log (PARAMS, "buffer = %s\n", buffer);
          ibelt_log (PARAMS, "la commande CGATT est KO \n");
          reading = 0;
          response = ERROR;
        }
        }
      return response;
    }
    
    int
    send_COPS_COMMAND (int fd)
    {
      unsigned char buffer[1000] = { 0 };
      int nb_read = 0;
      int nb_write = -1;
      int i = 0;
      int rep = -1;
      int reading = 1;
      int response = FAIL;
      nb_write = write (fd, COPS_COMMAND, sizeof (COPS_COMMAND));
      if (nb_write < 1)
        {
          ibelt_log (PARAMS, "erreur de l'ecriture de CPIN\n");
          return 1;
        }
      tcflush (fd, TCIFLUSH);	/* Discabufferrds old data in the rx buffer */
    
    
      while (reading && nb_read < sizeof (buffer))
        {
    
          rep = read (fd, buffer + nb_read, sizeof (char));
          if (rep < 1)
        {
          break;
        }
          if (*(buffer + nb_read) == '\0')
        {
          *(buffer + nb_read) = '\r';
        }
          buffer[++nb_read] = '\0';
    
          if (strstr (buffer, "OK") && strstr (buffer, "+COPS:"))
        {
          ibelt_log (PARAMS, "buffer = %s\n", buffer);
          ibelt_log (PARAMS, "la commande COPS est OK \n");
          reading = 0;
          response = SUCCESS;
        }
          else if (strstr (buffer, "ERROR"))
        {
          ibelt_log (PARAMS, "buffer = %s\n", buffer);
          ibelt_log (PARAMS, "la commande COPS est KO \n");
          reading = 0;
          response = ERROR;
        }
        }
      return response;
    }
    
    int
    send_CPIN_COMMAND (int fd)
    {
    
      unsigned char buffer[1000] = { 0 };
      int nb_read = 0;
      int nb_write = -1;
      int i = 0;
      int rep = -1;
      int reading = 1;
      int response = FAIL;
      nb_write = write (fd, CPIN_COMMAND, sizeof (CPIN_COMMAND));
      if (nb_write < 1)
        {
          ibelt_log (PARAMS, "erreur de l'ecriture de CPIN\n");
          return 1;
        }
      tcflush (fd, TCIFLUSH);	/* Discabufferrds old data in the rx buffer */
    
    
      while (reading && nb_read < sizeof (buffer))
        {
    
          rep = read (fd, buffer + nb_read, sizeof (char));
          if (rep < 1)
        {
          break;
        }
          if (*(buffer + nb_read) == '\0')
        {
          *(buffer + nb_read) = '\r';
        }
          buffer[++nb_read] = '\0';
    
          if (strstr (buffer, "OK") && strstr (buffer, "+CPIN: READY"))
        {
          ibelt_log (PARAMS, "buffer = %s\n", buffer);
          ibelt_log (PARAMS, "la commande CPIN est OK \n");
          reading = 0;
          response = SUCCESS;
        }
          else if (strstr (buffer, "ERROR"))
        {
          ibelt_log (PARAMS, "buffer = %s\n", buffer);
          ibelt_log (PARAMS, "la commande CPIN est KO \n");
          reading = 0;
          response = ERROR;
        }
        }
      return response;
    }
    
    int
    send_AT_COMMAND (int fd)
    {
      unsigned char buffer[1000] = { 0 };
      int nb_read = 0;
      int nb_write = -1;
      int i = 0;
      int rep = -1;
      int reading = 1;
      int response = FAIL;
      nb_write = write (fd, AT_COMMAND, sizeof (AT_COMMAND));
      if (nb_write < 1)
        {
          ibelt_log (PARAMS, "erreur de l'ecriture de AT\n");
          return 1;
        }
      tcflush (fd, TCIFLUSH);
      while (reading && nb_read < sizeof (buffer))
        {
    
          rep = read (fd, buffer + nb_read, sizeof (char));
          if (rep < 1)
        {
          ibelt_log (PARAMS, "read failed \n");
          break;
        }
          if (*(buffer + nb_read) == '\0')
        {
          *(buffer + nb_read) = '\r';
        }
          buffer[++nb_read] = '\0';
    
          if (strstr (buffer, "OK"))
        {
          ibelt_log (PARAMS, "buffer = %s\n", buffer);
          ibelt_log (PARAMS, "la commande AT est OK \n");
          reading = 0;
          response = SUCCESS;
        }
          else if (strstr (buffer, "ERROR"))
        {
          ibelt_log (PARAMS, "buffer = %s\n", buffer);
          ibelt_log (PARAMS, "la commande AT est KO \n");
          reading = 0;
          response = ERROR;
        }
        }
      return response;
    }
    
    int
    main (void)
    {
      int fd;
      struct termios newtio;
    
      fd = open (FIBOCOM_DEVICE, O_RDWR | O_NOCTTY);
    
      if (fd == -1)
        ibelt_log (PARAMS, "\n  Error! in Opening /dev/ttyACM0  \n");
      else
        ibelt_log (PARAMS, "\n  /dev/ttyACM0 Opened Successfully \n");
    
      bzero (&newtio, sizeof (newtio));	/* clear struct for new port settings */
      newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
      newtio.c_iflag = IGNPAR | ICRNL;
      newtio.c_oflag = 0;
      newtio.c_lflag = 0;
      newtio.c_cc[VTIME] = 100;	/* inter-character timer unused  10 secondes */
      newtio.c_cc[VMIN] = 0;	/* blocking read until 1 character arrives */
    
    
      tcflush (fd, TCIFLUSH);
      tcsetattr (fd, TCSANOW, &newtio);
    
      unsigned char read_buffer = 0;
      int bytes_read = 0;
      int i = 0;
      int count = 0;
      int ret = 1;
    
      while (SUCCESS != send_AT_COMMAND (fd) && count < 3)
        {
          ibelt_log (PARAMS, "send_AT_COMMAND tentative %d\n", count);
          count++;
          sleep (2);
        }
      if (count > 3)
        {
          ibelt_log (PARAMS, "send_AT_COMMAND FAILED or ERROR\n");
          return 1;
        }
      else
        {
          ibelt_log (PARAMS, "send_AT_COMMAND SUCCESS\n");
          count = 0;
        }
    
      while (SUCCESS != send_CPIN_COMMAND (fd) && count < 3)
        {
          ibelt_log (PARAMS, "send_CPIN_COMMAND tentative %d\n", count);
          count++;
          sleep (2);
        }
      if (count > 3)
        {
          ibelt_log (PARAMS, "send_CPIN_COMMAND FAILED or ERROR\n");
          return 1;
        }
      else
        {
          ibelt_log (PARAMS, "send_CPIN_COMMAND SUCCESS\n");
          count = 0;
        }
    
      while (SUCCESS != send_COPS_COMMAND (fd) && count < 3)
        {
          ibelt_log (PARAMS, "send_COPS_COMMAND tentative %d\n", count);
          count++;
          sleep (2);
        }
      if (count > 3)
        {
          ibelt_log (PARAMS, "send_COPS_COMMAND FAILED or ERROR\n");
          return 1;
        }
      else
        {
          ibelt_log (PARAMS, "send_COPS_COMMAND SUCCESS\n");
          count = 0;
        }
    
      while (SUCCESS != send_CSQ_COMMAND (fd) && count < 3)
        {
          ibelt_log (PARAMS, "send_CSQ_COMMAND tentative %d\n", count);
          count++;
          sleep (2);
        }
      if (count > 3)
        {
          ibelt_log (PARAMS, "send_CSQ_COMMAND FAILED or ERROR\n");
          return 1;
        }
      else
        {
          ibelt_log (PARAMS, "send_CSQ_COMMAND SUCCESS\n");
          count = 0;
        }
      while (SUCCESS != send_CGATT_COMMAND (fd) && count < 3)
        {
    
          ibelt_log (PARAMS, "send_CGATT_COMMAND tentative %d\n", count);
          count++;
          sleep (2);
        }
      if (count > 3)
        {
          ibelt_log (PARAMS, "send_CGATT_COMMAND failed\n");
          return 1;
        }
      else
        {
          count = 0;
        }
    
      while (SUCCESS != send_XDATACHANNEL_COMMAND (fd) && count < 3)
        {
          ibelt_log (PARAMS, "send_XDATACHANNEL_COMMAND tentative %d\n", count);
          count++;
          sleep (2);
        }
      if (count > 3)
        {
          ibelt_log (PARAMS, "send_XDATACHANNEL_COMMAND failed\n");
          return 1;
        }
      else
        {
          count = 0;
        }
    
      while (SUCCESS != send_CGDATA_COMMAND (fd) && count < 3)
        {
          ibelt_log (PARAMS, "send_CGDATA_COMMAND tentative %d\n", count);
          count++;
          sleep (2);
        }
      if (count > 3)
        {
          ibelt_log (PARAMS, "send_CGDATA_COMMAND failed\n");
          return 1;
        }
      else
        {
          count = 0;
        }
    
      while (SUCCESS != send_CGPADDR_COMMAND (fd) && count < 3)
        {
          ibelt_log (PARAMS, "send_CGPADDR_COMMAND tentative %d\n", count);
          count++;
          sleep (2);
        }
      if (count > 3)
        {
          ibelt_log (PARAMS, "send_CGPADDR_COMMAND failed\n");
          return 1;
        }
      else
        {
          count = 0;
        }
      while (SUCCESS != send_XDNS_COMMAND (fd) && count < 3)
        {
          ibelt_log (PARAMS, "send_XDNS_COMMAND tentative %d\n", count);
          count++;
          sleep (2);
        }
      if (count > 3)
        {
          ibelt_log (PARAMS, "send_CGPADDR_COMMAND failed\n");
          return 1;
        }
    
      close (fd);
      return 0;
    }
    
