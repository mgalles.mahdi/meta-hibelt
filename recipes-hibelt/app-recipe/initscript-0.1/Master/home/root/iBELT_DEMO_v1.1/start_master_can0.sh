#!/bin/sh

# X11 DISPLAY
export DISPLAY=:0.0                                                       

# COMS UART device
export IBT_COMS_UART=/dev/ttymxc2

# Execution time in seconds
export EXEC_TIME=36000

# Cloud device Id
export IBT_CLOUD_DEVICE_ID=1

# CAN interface name
export IBT_CAN_IF=can0

# I2C port number
export IBT_I2C_PORT_NUM=1

echo " >> Start iBELT master application with" $IBT_CAN_IF "for" $EXEC_TIME "seconds"
echo "     COMS UART =" $IBT_COMS_UART
echo "     Cloud device ID =" $IBT_CLOUD_DEVICE_ID
echo "     I2C port number =" $IBT_I2C_PORT_NUM
cd /home/root/iBELT_DEMO_v1.1
./iBELT_GUI M $IBT_COMS_UART $EXEC_TIME $IBT_CLOUD_DEVICE_ID $IBT_CAN_IF $IBT_I2C_PORT_NUM
