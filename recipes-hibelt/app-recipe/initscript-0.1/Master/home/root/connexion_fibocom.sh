#!/bin/sh
ROOT_DIR=/home/root
LOGS_DIR=$ROOT_DIR/logs
LOG_FILE=$LOGS_DIR/connexion_fibocom_`date +"%Y%m%d_%H%M%S"`.log
count=0
bin=KO
ping_count=0
if [ ! -e $LOGS_DIR ]; then
  mkdir -p $LOGS_DIR
  echo "$LOGS_DIR created"
fi
 
  while ((count<3))
  do
    /home/root/connexion_fibocom > $LOG_FILE 2>&1
    if [ $? -eq 0 ]
    then
      echo "bin OK"  >> $LOG_FILE 2>&1                                                 
      bin=OK                                                        
      break
   fi
   ((count=+1))      
 done


if [ $bin = "OK" ]
then
  echo "nameserver `cat /tmp/dns1.log`" >> /etc/resolv.conf
  echo "nameserver `cat /tmp/dns2.log`" >> /etc/resolv.conf
  ifconfig usb0 `cat /tmp/ipfile.log` netmask 255.255.255.255 -arp
  ip r add `cat /tmp/ipfile.log` dev usb0
  ip r add 0.0.0.0/0 via `cat /tmp/ipfile.log` dev usb0
  route add default gw `cat /tmp/ipfile.log`
   

  while ((ping_count<3))
  do                    
  ping -I usb0 -c 1 8.8.8.8 >> $LOG_FILE 2>&1 
    if [ $? -eq 0 ]
    then           
      echo "ping OK"
      touch /tmp/4G_PING_OK >> $LOG_FILE 2>&1
      break  
   else
     sleep 3    
   fi          
   ((ping_count=+1))
 done

else
  echo "pb binaire" >> $LOG_FILE 2>&1
  exit 1
fi
exit 0

