SUMMARY = "Slave recipe"
DESCRIPTION = ""
LICENSE = "CLOSED"
PR = "r3"

M_S_DEVICE= "SLAVE"
ROOT_DIR = "/home/root"
SYSTMD_DIR = "/etc/systemd/system"
USR_SHARE_DIR = "/usr/share/X11/xorg.conf.d/"
IBELT_DEMO_V1.1_DIR="${ROOT_DIR}/iBELT_DEMO_v1.1"
SRC_URI =  " \
    file://Slave/home/root/ipFixe.sh \
    file://Slave/home/root/rotate.sh \
    file://Slave/Services/init_ipFixe.service \
    file://Slave/Services/init_rotate_screen.service \
    file://Slave/Services/start_demo_slave.service \
    file://Slave/x11/10-quirks.conf \
    file://Slave/home/root/iBELT_DEMO_v1.1/iBELT_GUI \
    file://Slave/home/root/iBELT_DEMO_v1.1/start_slave.sh \
"

do_compile () {
}

do_install () {

    install -d ${D}${SYSTMD_DIR}
    install -m 0644 ${WORKDIR}/Slave/Services/*.service ${D}${SYSTMD_DIR}

    install -d ${D}${ROOT_DIR}
    install -m 0777 ${WORKDIR}/Slave/home/root/*.sh ${D}${ROOT_DIR}

    install -d ${D}${USR_SHARE_DIR}
    install -m 0644 ${WORKDIR}/Slave/x11/*.conf ${D}${USR_SHARE_DIR}

    install -d ${D}${IBELT_DEMO_V1.1_DIR}
    install -m 0644 ${WORKDIR}/Slave/home/root/iBELT_DEMO_v1.1/iBELT_GUI ${D}${IBELT_DEMO_V1.1_DIR}
    install -m 0777 ${WORKDIR}/Slave/home/root/iBELT_DEMO_v1.1/start_slave.sh ${D}${IBELT_DEMO_V1.1_DIR}

}

NATIVE_SYSTEMD_SUPPORT = "1"
SYSTEMD_PACKAGES = "${PN}"
SYSTEMD_SERVICE_${PN} = "init_ipFixe.service"
SYSTEMD_SERVICE_${PN} += "init_rotate_screen.service"
SYSTEMD_SERVICE_${PN} += "start_demo_slave.service"
FILES_${PN} += "${SYSTMD_DIR}/*"
FILES_${PN} += "${USR_SHARE_DIR}/*"
FILES_${PN} += "${ROOT_DIR}/*"
FILES_${PN} += "${IBELT_DEMO_V1.1_DIR}/*"
inherit systemd

